using Xunit;
using Marten;
using truename.Application.Domain;
using System;
using System.Linq;
using truename.Application;
using truename.Application.Components;
using truename.Application.Data;
using truename.Application.Helpers;
using System.Collections.Generic;

namespace truename.Tests.Domain
{
	public class MatchTests
	{
		private string connectionString;
		private readonly IEventPublisher bus;
		private readonly IDocumentStore store;
		private readonly Repository repository;

		private readonly Guid playerId = Guid.NewGuid();
		private readonly Guid player2Id = Guid.NewGuid();
		private readonly string playerName = "tron_travolta";
		private readonly Format format = Formats.Lookup["Legacy"];

		public MatchTests()
		{
			connectionString = "host=localhost;port=5454;database=marten_test;password=Password12!;username=postgres";
			store = DocumentStore.For(_ =>
			{
				_.Connection(connectionString);
				_.Events.InlineProjections.AggregateStreamsWith<Match>();
				_.DdlRules.TableCreation = CreationStyle.DropThenCreate;
			});

			store.Advanced.Clean.CompletelyRemoveAll();
			store.Advanced.Clean.DeleteAllDocuments();

			bus = new Bus();
			repository = new Repository(store, bus);
		}

		[Fact]
		public void Join_adds_Entity_with_Player_Component()
		{
			var match = new Match(Guid.NewGuid(), format);
			match.Join(playerId, playerName, new[] { "Force of Will" });
			repository.Store(match);

			var match2 = repository.Load<Match>(match.Id);
			Assert.Single(match2.Entities);

			var entity = match2.Entities.First();
			var player = entity.Components.OfType<Player>().First();
			Assert.Equal(playerId, player.PlayerId);
		}

		[Fact]
		public void SetLifeTotal_sets_LifeTotal_on_Player_Component()
		{
			var startingLifeTotal = 20;
			var match = new Match(Guid.NewGuid(), format);
			match.Join(playerId, playerName, Enumerable.Empty<string>());

			match.SetLifeTotal(playerId, startingLifeTotal);

			var player = match.GetPlayer(playerId);
			Assert.Equal(startingLifeTotal, player.LifeTotal);
		}

		[Fact]
		public void SetTurnOrder_throws_Exception_if_ids_dont_match()
		{
			var match = new Match(Guid.NewGuid(), format);
			match.Join(playerId, playerName, new[] { "Force of Will" });
			match.Join(playerId, playerName, new[] { "Thoughtseize" });

			Assert.Throws<InvalidOperationException>(
				() => match.SetTurnOrder(new[] { Guid.NewGuid() })
			);
		}

		public class Concede
		{
			private readonly Guid playerId = Guid.NewGuid();
			private readonly string playerName = "tron_travolta";
			private readonly Format format = Formats.Lookup["Legacy"];

			[Fact]
			public void Concede_changes_Player_Component()
			{
				var match = new Match(Guid.NewGuid(), format);
				match.Join(playerId, playerName, new[] { "Force of Will" });

				match.Concede(playerId);

				var p = match.GetPlayer(playerId);
				Assert.True(p.Conceded);
			}

			[Fact]
			public void Concede_throws_Exception_if_Player_already_Conceded()
			{
				var match = new Match(Guid.NewGuid(), format);
				match.Join(playerId, playerName, new[] { "Force of Will" });

				match.Concede(playerId);

				var p = match.GetPlayer(playerId);
				Assert.Throws<Exception>(() => match.Concede(playerId));
			}
		}

		public class ShuffleLibrary
		{
			private readonly Guid playerId = Guid.NewGuid();
			private readonly string playerName = "tron_travolta";
			private readonly Format format = Formats.Lookup["Legacy"];

			[Fact]
			public void ShuffleLibrary_changes_Library_order_in_Player_Component()
			{
				var match = new Match(Guid.NewGuid(), format);
				var deckList = Enumerable.Repeat("Karn, Liberated", 4)
					.Concat(Enumerable.Repeat("Thoughtknot Seer", 4));
				match.Join(playerId, playerName, deckList);

				match.ShuffleLibrary(playerId, deckList.Reverse());

				var player = match.GetPlayer(playerId);
				var library = player.Library;
				Assert.Equal("Thoughtknot Seer", library.First());
			}

			[Fact]
			public void ShuffleLibrary_throws_if_given_library_doesnt_match()
			{
				var match = new Match(Guid.NewGuid(), format);
				var deckList = Enumerable.Repeat("Karn, Liberated", 4);
				var notTheDeckList = Enumerable.Repeat("Thoughtseize", 4);
				match.Join(playerId, playerName, deckList);

				Assert.Throws<Exception>(
					() => match.ShuffleLibrary(playerId, notTheDeckList)
				);
			}
		}

		public class DrawCards
		{
			private readonly Guid playerId = Guid.NewGuid();
			private readonly string playerName = "tron_travolta";
			private readonly Format format = Formats.Lookup["Legacy"];

			[Fact]
			public void DrawCards_moves_cards_from_Player_Component_Library_to_Hand()
			{
				var startingHandSize = 7;
				var match = new Match(Guid.NewGuid(), format);
				var deckList = Enumerable.Repeat("Karn, Liberated", 4)
					.Concat(Enumerable.Repeat("Thoughtknot Seer", 4));
				match.Join(playerId, playerName, deckList);
				var player = match.GetPlayer(playerId);

				match.DrawCards(playerId, startingHandSize);

				var hand = player.Hand;
				var library = player.Library;
				Assert.Equal(startingHandSize, hand.Count);
				Assert.Single(library);
			}

			[Fact]
			public void DrawCards_throws_if_cards_drawn_exceeds_library_length()
			{
				var match = new Match(Guid.NewGuid(), format);
				var deckList = Enumerable.Repeat("Karn, Liberated", 4);
				match.Join(playerId, playerName, deckList);

				Assert.Throws<Exception>(() => match.DrawCards(playerId, 5));
			}
		}
	}
}