﻿using System.Collections.Generic;
using System.Linq;
using truename.Application.Helpers;
using Xunit;

namespace truename.Tests
{
	public class HelpersTests
	{
		[Fact]
		public void NextInLoop_starts_at_beginning_of_collection_when_index_passes_the_end()
		{
			var cards = new List<string>(new[]
			{
				"Thoughtseize",
				"Bloodghast",
				"Fatal Push"
			});

			var first = cards.First();
			var next = cards.NextInLoop(3);
			Assert.Equal(first, next);
		}
	}
}
