﻿using System;
using System.Threading.Tasks;
using Marten;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using truename.Application.Domain;

namespace truename.Application
{
	class Program
	{
		static async Task Main(string[] args)
		{
			using IHost host = CreateHostBuilder(args).Build();

			var configuration = GetConfiguration(host);
			var connectionString = configuration.GetValue<string>("ConnectionStrings:martenLocal");
			var store = DocumentStore.For(_ =>
			{
				_.Connection(connectionString);
				_.Events.InlineProjections.AggregateStreamsWith<Match>();
			});

			var messageBus = new Bus();
			var repo = new Repository(store, messageBus);

			await host.RunAsync();
		}

		static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureAppConfiguration((hostingContext, configuration) =>
				{
					configuration.Sources.Clear();
					var env = hostingContext.HostingEnvironment;
					configuration
						.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
						.AddJsonFile("$appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
				});

		static IConfiguration GetConfiguration(IHost host) =>
			(IConfiguration)host.Services.GetService(typeof(IConfiguration));
	}
}
