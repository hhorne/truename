using System;

namespace truename.Application.Events
{
	public class PlayerConceded
	{
		public Guid PlayerId { get; set; }

		public PlayerConceded(Guid playerId)
		{
			PlayerId = playerId;
		}
	}
}