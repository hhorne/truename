using System;

namespace truename.Application.Events
{
	public class CardsDrawn
	{
		public Guid PlayerId;
		public int Number;

		public CardsDrawn(Guid playerId, int number)
		{
			PlayerId = playerId;
			Number = number;
		}
	}
}