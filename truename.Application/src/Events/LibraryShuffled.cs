using System;
using System.Collections.Generic;

namespace truename.Application.Events
{
	public class LibraryShuffled
	{
		public Guid PlayerId { get; set; }
		public IEnumerable<string> ShuffledLibrary { get; set; }

		public LibraryShuffled(Guid playerId, IEnumerable<string> shuffledLibrary)
		{
			PlayerId = playerId;
			ShuffledLibrary = shuffledLibrary;
		}
	}
}