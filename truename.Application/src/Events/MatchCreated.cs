using System;
using truename.Application.Domain;

namespace truename.Application.Events
{
	public class MatchCreated
	{
		public Guid Id { get; set; }
		public Format Format { get; set; }

		public MatchCreated(Guid id, Format format)
		{
			Id = id;
			Format = format;
		}
	}
}