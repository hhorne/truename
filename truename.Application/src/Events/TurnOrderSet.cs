using System;
using System.Collections.Generic;
using truename.Application.Domain;

namespace truename.Application.Events
{
	public class TurnOrderSet
	{
		public Guid[] TurnOrder { get; set; }

		public TurnOrderSet(Guid[] turnOrder)
		{
			TurnOrder = turnOrder;
		}
	}
}