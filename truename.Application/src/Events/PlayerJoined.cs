using System;
using System.Collections.Generic;

namespace truename.Application.Events
{
	public class PlayerJoined
	{
		public Guid PlayerId { get; set; }
		public string Name { get; set; }
		public IEnumerable<string> DeckList { get; set; }

		public PlayerJoined(Guid playerId, string name, IEnumerable<string> deckList)
		{
			PlayerId = playerId;
			Name = name;
			DeckList = deckList;
		}
	}
}