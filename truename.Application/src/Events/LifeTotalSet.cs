using System;

namespace truename.Application.Events
{
	public class LifeTotalSet
	{
		public Guid PlayerId { get; set; }
		public int LifeTotal { get; set; }

		public LifeTotalSet(Guid playerId, int lifeTotal)
		{
			PlayerId = playerId;
			LifeTotal = lifeTotal;
		}
	}
}