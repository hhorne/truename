using System;

namespace truename.Application
{
	public abstract class Component
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public Guid EntityId { get; private set; }

		public void Register(Guid entityId)
		{
			EntityId = entityId;
		}
	}
}