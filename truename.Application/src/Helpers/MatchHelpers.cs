using System;
using System.Collections.Generic;
using System.Linq;
using truename.Application.Components;
using truename.Application.Domain;

namespace truename.Application.Helpers
{
	public static class MatchHelpers
	{
		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			foreach (T item in source)
				action(item);
		}

		public static bool HasComponent<T>(Entity entity) where T : Component =>
			entity.Components.OfType<T>().Any();

		public static IEnumerable<T> GetComponents<T>(this IEnumerable<Entity> entities) where T : Component =>
			entities.SelectMany(e => e.Components.OfType<T>());

		public static IEnumerable<Player> GetPlayers(this Match match, Func<Player, bool> predicate = null)
		{
			var entities = match.Entities;
			var players = entities.GetComponents<Player>();
			if (predicate is not null)
			{
				players.Where(predicate);
			}
			return players ?? Enumerable.Empty<Player>();
		}

		public static Player GetPlayer(this Match match, Guid playerId) =>
			match.GetPlayers().Single(p => p.PlayerId == playerId);

		public static Player GetActivePlayer(this Match match) =>
			match.GetPlayer(match.ActivePlayerId);

		// Fisher-Yates Shuffle
		// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
		public static T[] Shuffle<T>(this Random rng, T[] array)
		{
			int n = array.Length;
			T[] result = (T[])array.Clone();
			while (n > 1)
			{
				int k = rng.Next(n--);
				T temp = result[n];
				result[n] = result[k];
				result[k] = temp;
			}

			return result;
		}

		public static Guid GetStartingPlayer(this Match match)
		{
			var playerId = match.TurnOrder.FirstOrDefault();
			var startingPlayer = match.GetPlayer(playerId);
			return startingPlayer.PlayerId;
		}

		// Get the next value in an Enum sequence, or if at the end, loop back
		// to the first value.
		public static T Next<T>(this T src) where T : struct
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException($"Argument {typeof(T).FullName} is not an Enum");
			}

			T[] vals = (T[])Enum.GetValues(src.GetType());
			int nextIndex = Array.IndexOf(vals, src) + 1;
			return (vals.Length == nextIndex)
				? vals[0]
				: vals[nextIndex];
		}

		public static T NextInLoop<T>(this ICollection<T> src, int position) =>
			src.ElementAt(position % src.Count);
	}
}