using System;
using System.Linq;
using Marten;
using truename.Application.Domain;

namespace truename.Application
{
	public class Repository
	{
		private readonly IDocumentStore store;
		private readonly IEventPublisher publisher;

		public Repository(IDocumentStore store, IEventPublisher publisher)
		{
			this.store = store;
			this.publisher = publisher;
		}

		public void Store<T>(T aggregate) where T : AggregateRoot
		{
			using (var session = store.OpenSession())
			{
				// Take non-persisted events, push them to the event stream, indexed by the aggregate ID
				var events = aggregate.GetUncommittedEvents().ToArray();
				session.Events.Append(aggregate.Id, aggregate.Version, events);
				session.SaveChanges();

				foreach (var @event in events)
				{
					publisher.Publish(@event);
				}
			}
			// Once successfully persisted, clear events from list of uncommitted events
			aggregate.ClearUncommittedEvents();
		}

		public T Load<T>(Guid id)
		{
			using (var session = store.LightweightSession())
			{
				return session.Load<T>(id);
			}
		}

		public T AggregateStream<T>(Guid id, int? version = null) where T : AggregateRoot
		{
			using (var session = store.LightweightSession())
			{
				var aggregate = session.Events.AggregateStream<T>(id, version ?? 0);
				return aggregate ?? throw new InvalidOperationException($"No aggregate by id {id}.");
			}
		}
	}
}