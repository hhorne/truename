using System.Collections.Generic;
using truename.Application.Domain;

namespace truename.Application.Data
{
	public class Formats
	{
		public static readonly Dictionary<string, Format> Lookup = new Dictionary<string, Format>()
		{
			{ "Standard", new Format { FormatId = "Standard", FormatType = FormatTypes.Constructed } },
			{ "Pioneer", new Format { FormatId = "Pioneer", FormatType = FormatTypes.Constructed } },
			{ "Modern", new Format { FormatId = "Modern", FormatType = FormatTypes.Constructed } },
			{ "Legacy", new Format { FormatId = "Legacy", FormatType = FormatTypes.Constructed } },
			{ "Vintage", new Format { FormatId = "Vintage", FormatType = FormatTypes.Constructed } },
		};
	}
}