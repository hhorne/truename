using System;
using System.Collections.Generic;

namespace truename.Application
{
	public class Entity
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public IEnumerable<Component> Components { get; set; }

		public Entity(IEnumerable<Component> components)
		{
			Components = components;
			foreach (var c in Components)
			{
				c.Register(Id);
			}
		}
	}
}