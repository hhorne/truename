namespace truename.Application.Domain
{
	public enum FormatTypes
	{
		None = 0,
		Limited,
		Constructed,
		Commander,
		Custom,
	}
}