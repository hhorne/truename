using System.Collections.Generic;
using System.Linq;

namespace truename.Application.Domain
{
	public class Library<T> : List<T>
	{
		public Library(IEnumerable<T> c) : base(c) { }

		public IEnumerable<T> TakeTop(int n) => this.TakeLast(n);

		public void PutOnBottom(T obj) => Insert(0, obj);

		public void PutOnBottom(T[] arr) => InsertRange(0, arr);

		public void PutOnTop(T obj, int? offset = null) => PutOnTop(new[] { obj }, offset);

		public void PutOnTop(T[] arr, int? offset = null)
		{
			if (offset.HasValue)
			{
				var top = Count - 1;
				var position = top - offset.Value;
				InsertRange(position, arr);
			}

			AddRange(arr);
		}
	}
}