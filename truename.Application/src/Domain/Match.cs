using System;
using System.Collections.Generic;
using System.Linq;
using truename.Application.Components;
using truename.Application.Events;
using truename.Application.Helpers;

namespace truename.Application.Domain
{
	public class Match : AggregateRoot
	{
		public Format Format { get; set; }

		public List<Entity> Entities { get; set; } = new();

		public List<Guid> TurnOrder { get; set; } = new();

		public Guid ActivePlayerId { get; set; }

		public Guid PriorityHolder { get; set; }


		public Match() { }

		public Match(Guid id, Format format)
		{
			var @event = new MatchCreated(id, format);
			Apply(@event);
			AddUncommittedEvent(@event);
		}

		public void Apply(MatchCreated @event)
		{
			Id = @event.Id;
			Format = @event.Format;
		}

		public void Join(Guid playerId, string name, IEnumerable<string> deckList)
		{
			var @event = new PlayerJoined(playerId, name, deckList);
			Apply(@event);
			AddUncommittedEvent(@event);
		}

		public void Apply(PlayerJoined @event)
		{
			Entities.Add(new Entity(new[]
			{
				new Player(
					@event.PlayerId,
					@event.Name,
					@event.DeckList
				)
			}));
		}

		public void Concede(Guid playerId)
		{
			var player = this.GetPlayer(playerId);
			if (player.Conceded)
			{
				throw new Exception("Player already conceded");
			}

			var @event = new PlayerConceded(playerId);
			Apply(@event);
			AddUncommittedEvent(@event);
		}

		public void Apply(PlayerConceded @event)
		{
			var player = this.GetPlayer(@event.PlayerId);
			player.Conceded = true;
			TurnOrder = TurnOrder
				.Where(p => p != @event.PlayerId)
				.ToList();
		}

		public void SetTurnOrder(Guid[] turnOrder)
		{
			var players = this.GetPlayers(p => !p.Conceded)
				.Select(p => p.PlayerId)
				.ToList();

			if (!players.SequenceEqual(turnOrder))
			{
				throw new InvalidOperationException("Turn Order doesn't match remaining players.");
			}

			var @event = new TurnOrderSet(turnOrder);
			Apply(@event);
			AddUncommittedEvent(@event);
		}

		public void Apply(TurnOrderSet @event)
		{
			TurnOrder = @event.TurnOrder.ToList();
		}

		public void ShuffleLibrary(Guid playerId, IEnumerable<string> shuffledLibrary)
		{
			var player = this.GetPlayer(playerId);
			var library = player.Library;
			var differences = library.Except(shuffledLibrary);
			if (differences.Any())
			{
				throw new Exception("Cards in shuffled library do not match cards in library before shuffle.");
			}

			var @event = new LibraryShuffled(playerId, shuffledLibrary);
			Apply(@event);
			AddUncommittedEvent(@event);
		}

		public void Apply(LibraryShuffled @event)
		{
			var playerId = @event.PlayerId;
			var player = this.GetPlayer(playerId);
			player.Shuffle(@event.ShuffledLibrary);
		}

		public void SetLifeTotal(Guid playerId, int lifeTotal)
		{
			var player = this.GetPlayer(playerId);
			if (player.Conceded)
			{
				throw new Exception("Can't change life total of players already conceded from the game.");
			}

			var @event = new LifeTotalSet(playerId, lifeTotal);
			Apply(@event);
			AddUncommittedEvent(@event);
		}

		public void Apply(LifeTotalSet @event)
		{
			var playerId = @event.PlayerId;
			var player = this.GetPlayer(playerId);
			player.LifeTotal = @event.LifeTotal;
		}

		public void DrawCards(Guid playerId, int number)
		{
			var player = this.GetPlayer(playerId);
			var library = player.Library;

			if (library.Count < number)
			{
				throw new Exception("Library doesn't have enough cards to draw.");
			}

			var @event = new CardsDrawn(playerId, number);
			Apply(@event);
			AddUncommittedEvent(@event);
		}

		public void Apply(CardsDrawn @event)
		{
			var player = this.GetPlayer(@event.PlayerId);
			var library = player.Library;
			var hand = player.Hand;
			var topIndex = library.Count - 1;
			var cardsDrawn = library.TakeTop(@event.Number);
			hand.AddRange(cardsDrawn);
			library.RemoveRange(topIndex - @event.Number, @event.Number);
		}
	}
}