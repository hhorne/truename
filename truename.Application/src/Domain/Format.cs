using System;

namespace truename.Application.Domain
{
	public class Format
	{
		public FormatTypes FormatType { get; set; }
		public string FormatId { get; set; }
	}
}