namespace truename.Application.Domain.Turns
{
	public enum TurnPhases
	{
		Beginning,
		PreCombat,
		Combat,
		PostCombat,
		Ending,
	}
}