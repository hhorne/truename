namespace truename.Application.Domain
{
	public enum TurnSteps
	{
		Untap,
		Upkeep,
		Draw,
		Main1,
		BeginCombat,
		DeclareAttackers,
		DeclareBlocks,
		Damage,
		EndCombat,
		Main2,
		End,
		Cleanup,
	}
}