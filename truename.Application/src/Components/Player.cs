using System;
using System.Collections.Generic;
using truename.Application.Domain;

namespace truename.Application.Components
{
	public class Player : Component
	{
		public Guid PlayerId { get; set; }
		public string Name { get; set; }
		public bool Conceded { get; set; }
		public IEnumerable<string> DeckList { get; set; }
		public Library<string> Library { get; set; }
		public List<string> Hand { get; set; } = new();
		public int LifeTotal { get; set; } = new();
		public int TurnNumber { get; set; } = 1;

		public Player(Guid playerId, string name, IEnumerable<string> deckList)
		{
			PlayerId = playerId;
			Name = name;
			DeckList = deckList;
			Library = new Library<string>(deckList);
		}

		public void Shuffle(IEnumerable<string> shuffledLibrary)
		{
			Library = new Library<string>(shuffledLibrary);
		}
	}
}