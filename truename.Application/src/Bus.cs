using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace truename.Application
{
	using MessageHandlers = System.Collections.Generic.List<System.Action<System.Object>>;

	public interface IEventPublisher
	{
		void Publish<T>(T @event);
	}

	public class Bus : IEventPublisher
	{
		public interface IMessage { }
		public class Command : IMessage { }
		public class Event : IMessage { }

		private readonly Dictionary<Type, MessageHandlers> routes = new();
		public void RegisterHandler<T>(Action<T> handler)
		{
			MessageHandlers handlers;
			if (!routes.TryGetValue(typeof(T), out handlers))
			{
				handlers = new MessageHandlers();
				routes.Add(typeof(T), handlers);
			}

			handlers.Add(x => handler((T)x));
		}

		public void Send<T>(T command) where T : Command
		{
			MessageHandlers handlers;
			if (routes.TryGetValue(typeof(T), out handlers))
			{
				if (handlers.Count != 1)
				{
					throw new InvalidOperationException("Cannot send a Command to more than one handler.");
				}

				var handler = handlers.FirstOrDefault();
				handler(command);
			}
		}

		public void Publish<T>(T @event)
		{
			MessageHandlers handlers;
			if (!routes.TryGetValue(@event.GetType(), out handlers))
			{
				return;
			}

			foreach (var h in handlers)
			{
				//dispatch on thread pool for added awesomeness
				var handler = h;
				ThreadPool.QueueUserWorkItem(x => handler(@event));
			}
		}
	}
}